# promize-lokka

Block generating  service of promize app. When starting the service it will creates 
cassandra database, table and elastic indexes with elassandra api

## elastic index

### transactions

```
{
  "settings":{
    "keyspace": "mystiko"
  },
  "mappings": {
    "transactions" : {
      "discover" : ".*"
    }
  }
}
```

### blocks

```
{
  "settings":{
    "keyspace": "mystiko"
  },
  "mappings": {
    "blocks" : {
      "discover" : ".*"
    }
  }
}
```

### cluster/index state

```
http://localhost:9200/_cluster/state?pretty=true
http://localhost:9200/blocks?pretty=true
http://localhost:9200/transactions?pretty=true
```

## contracts

```
### create accounts
curl -XPOST "http://10.4.1.70:7070/promize" -d '{"Uid":"69B12057-40D0-41B1-9965-52FE1FC538DE","Type": "INIT", "FromAddress":"2222", "ToAddress": "2222", "Amount": 1000}'
curl -XPOST "http://10.4.1.70:7070/promize" -d '{"Uid":"DC09475D-F866-483F-A977-DA60485ADEE1","Type": "INIT", "FromAddress":"1111", "ToAddress": "1111", "Amount": 3000}'

### transfer amount
curl -XPOST "http://10.4.1.70:7070/promize" -d '{"Uid":"128CD7CE-C130-4867-BF8D-D04CA6F5D28C","Type": "PUT", "FromAddress":"2222", "ToAddress": "1111", "Amount": 500}'
curl -XPOST "http://10.4.1.70:7070/promize" -d '{"Uid":"7C349C1F-606B-4EDF-89A3-ACA9174290C4","Type": "PUT", "FromAddress":"1111", "ToAddress": "2222", "Amount": 1000}'
```
