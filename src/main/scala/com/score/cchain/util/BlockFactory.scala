package com.score.cchain.util

import com.score.cchain.config.AppConf
import com.score.cchain.protocol.{Block, Trans}

import scala.annotation.tailrec

object BlockFactory extends AppConf {

  def initBlock(trans: List[Trans], preHash: String): Block = {
    val timestamp = System.currentTimeMillis
    val merkleRoot = BlockFactory.merkleRoot(trans)
    val hash = BlockFactory.hash(timestamp.toString, merkleRoot, preHash)
    Block(miner = senzieName,
      hash = hash,
      transactions = trans,
      timestamp = timestamp,
      merkleRoot = merkleRoot,
      preHash = preHash
    )
  }

  def merkleRoot(transactions: List[Trans]): String = {
    @tailrec
    def merkle(ins: List[String], outs: List[String]): String = {
      ins match {
        case Nil =>
          // empty list
          if (outs.size == 1) outs.head
          else merkle(outs, List())
        case x :: Nil =>
          // one element list
          merkle(Nil, outs :+ RSAFactory.sha256(x + x))
        case x :: y :: l =>
          // have at least two elements in list
          // concat them and sign them
          merkle(l, outs :+ RSAFactory.sha256(x + y))
      }
    }

    merkle(transactions.map(t => RSAFactory.sha256(t.uid.toString)), List())
  }

  def hash(timestamp: String, markleRoot: String, preHash: String): String = {
    val p = timestamp + markleRoot + preHash
    RSAFactory.sha256(p)
  }

}
