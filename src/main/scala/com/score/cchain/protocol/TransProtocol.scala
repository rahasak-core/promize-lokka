package com.score.cchain.protocol

import java.util.UUID

import com.datastax.driver.core.utils.UUIDs

case class Trans(uid: UUID = UUIDs.timeBased,
                 typ: String,
                 fromAddress: String,
                 toAddress: String,
                 amount: Int,
                 digsig: String,
                 timestamp: Long = System.currentTimeMillis)
