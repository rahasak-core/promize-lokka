package com.score.cchain.protocol

case class Account(address: String, balance: Int)
