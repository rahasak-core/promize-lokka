package com.score.cchain

import akka.actor.ActorSystem
import com.score.cchain.actor.chain.BlockCreator
import com.score.cchain.actor.contract.PromizeContractActor
import com.score.cchain.util.{ChainFactory, MinerFactory}

object Main extends App {
  // create
  //  1. setup logging
  //  2. setup keys
  ChainFactory.setupLogging()
  ChainFactory.setupKeys()

  // create
  //  1. init config
  //  2. init schema
  //  3. init index
  MinerFactory.initConf()
  MinerFactory.initSchema()
  MinerFactory.initIndex()

  // start senz, block creator
  implicit val system = ActorSystem("mystiko")
  system.actorOf(BlockCreator.props, name = "BlockCreator")
  system.actorOf(PromizeContractActor.props, name = "Contractor")
}
