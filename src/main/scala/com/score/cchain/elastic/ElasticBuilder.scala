//package com.score.cchain.elastic
//
//import com.score.cchain.config.ElasticConf
//import org.elasticsearch.index.query.QueryBuilders
//
//object ElasticBuilder extends ElasticConf with ElasticCluster {
//
//  def search() = {
//    val builder = QueryBuilders.boolQuery()
//
//    // build termas
//    builder.should(QueryBuilders.wildcardQuery("from_address", "222*"))
//    builder.should(QueryBuilders.wildcardQuery("to_address", "222*"))
//    builder.must(QueryBuilders.wildcardQuery("to_address", "rs121ds"))
//
//    // extract all documents from search response
//    val searchBuilder = client
//      .prepareSearch("transactions")
//      .setTypes("transactions")
//      .setQuery(builder)
//
//    val resp = searchBuilder.execute().actionGet()
//    val hits = resp.getHits.getHits
//    println(hits.size)
//  }
//
//}
//
//object M extends App {
//  ElasticBuilder.search()
//}
