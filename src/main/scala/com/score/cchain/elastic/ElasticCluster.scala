package com.score.cchain.elastic

import java.net.InetAddress

import com.score.cchain.config.ElasticConf
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.TransportAddress
import org.elasticsearch.transport.client.PreBuiltTransportClient

trait ElasticCluster extends ElasticConf {
  lazy val settings = Settings.builder()
    .put("cluster.name", "document-storage")
    .build()
  val client = new PreBuiltTransportClient(settings)
  client.addTransportAddress(new TransportAddress(InetAddress.getByName("10.4.1.70"), 9300))
}

