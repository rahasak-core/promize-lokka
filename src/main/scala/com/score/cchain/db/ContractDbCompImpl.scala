package com.score.cchain.db

import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.querybuilder.QueryBuilder._
import com.score.cchain.config.CassandraConf
import com.score.cchain.protocol.Account
import com.score.cchain.util.MinerFactory

trait ContractDbCompImpl extends ContractDbComp with CassandraConf {

  val contractDb = new ContractDbImpl

  class ContractDbImpl extends ContractDb {
    override def init(account: Account): Unit = {
      // insert query
      val statement = QueryBuilder.insertInto(cassandraKeyspace, "accounts")
        .value("address", account.address)
        .value("balance", account.balance)

      MinerFactory.session.execute(statement)
    }

    override def put(address: String, amount: Int): Unit = {
      // update balance
      val statement = QueryBuilder.update(cassandraKeyspace, "accounts")
        .`with`(QueryBuilder.set("balance", amount))
        .where(QueryBuilder.eq("address", address))

      MinerFactory.session.execute(statement)
    }

    override def get(address: String): Option[Account] = {
      // select query
      val selectStmt = select()
        .all()
        .from(cassandraKeyspace, "accounts")
        .where(QueryBuilder.eq("address", address))
        .limit(1)

      // get first match
      val resultSet = MinerFactory.session.execute(selectStmt)
      val row = resultSet.one()

      // account object
      if (row != null) Option(Account(row.getString("address"), row.getInt("balance")))
      else None
    }

    def teransfer() = {

    }

    def delte() = {

    }
  }

}
