package com.score.cchain.db

import java.util.UUID

import com.datastax.driver.core.UDTValue
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.querybuilder.QueryBuilder._
import com.score.cchain.config.{CassandraConf, ElasticConf}
import com.score.cchain.elastic.ElasticCluster
import com.score.cchain.protocol._
import com.score.cchain.util.{MinerFactory, SenzLogger}
import org.elasticsearch.index.query.QueryBuilders

import scala.collection.JavaConverters._

trait ChainDbCompImpl extends ChainDbComp with CassandraConf with ElasticCluster with ElasticConf with SenzLogger {

  val chainDb = new ChainDbImpl

  class ChainDbImpl extends ChainDb {

    def createTrans(trans: Trans): Unit = {
      val statement = QueryBuilder.insertInto(cassandraKeyspace, "transactions")
        .value("id", trans.uid)
        .value("type", trans.typ)
        .value("from_address", trans.fromAddress)
        .value("to_address", trans.toAddress)
        .value("amount", trans.amount)
        .value("digsig", trans.digsig)
        .value("timestamp", trans.timestamp)

      MinerFactory.session.execute(statement)
    }

    def getTrans: List[Trans] = {
      // select query
      val selectStmt = select()
        .all()
        .from(cassandraKeyspace, "trans")

      // get all trans
      val resultSet = MinerFactory.session.execute(selectStmt)
      resultSet.all().asScala.map { row =>
        Trans(row.getUUID("uid"),
          row.getString("type"),
          row.getString("from_address"),
          row.getString("to_address"),
          row.getInt("amount"),
          row.getString("digsig"),
          row.getLong("timestamp")
        )
      }.toList
    }

    def deleteTrans(trans: List[Trans]): Unit = {
      for (t <- trans) {
        // delete query
        val delStmt = delete()
          .from(cassandraKeyspace, "trans")
          .where(QueryBuilder.eq("uid", t.uid))

        MinerFactory.session.execute(delStmt)
      }
    }

    def createPreHash(hash: String): Unit = {
      // insert query
      val statement = QueryBuilder.insertInto(cassandraKeyspace, "hashes")
        .value("hash", hash)

      MinerFactory.session.execute(statement)
    }

    def getPreHash: Option[String] = {
      // select query
      val selectStmt = select()
        .all()
        .from(cassandraKeyspace, "hashes")
        .limit(1)

      val resultSet = MinerFactory.session.execute(selectStmt)
      val row = resultSet.one()

      if (row != null) Option(row.getString("hash"))
      else None
    }

    def deletePreHash(): Unit = {
      MinerFactory.session.execute(s"TRUNCATE $cassandraKeyspace.hashes;")
    }

    def createBlock(block: Block): Unit = {
      // UDT
      val transType = MinerFactory.cluster.getMetadata.getKeyspace(cassandraKeyspace).getUserType("transaction")

      // transactions
      val transactions = block.transactions.map(t =>
        transType.newValue
          .setUUID("uid", t.uid)
          .setString("type", t.typ)
          .setString("from_address", t.fromAddress)
          .setString("to_address", t.toAddress)
          .setInt("amount", t.amount)
          .setString("digsig", t.digsig)
          .setLong("timestamp", t.timestamp)
      ).asJava

      // insert query
      val statement = QueryBuilder.insertInto(cassandraKeyspace, "blocks")
        .value("miner", block.miner)
        .value("id", block.id)
        .value("transactions", transactions)
        .value("timestamp", block.timestamp)
        .value("merkle_root", block.merkleRoot)
        .value("pre_hash", block.preHash)
        .value("hash", block.hash)

      MinerFactory.session.execute(statement)
    }

    def getBlock(miner: String, id: UUID): Option[Block] = {
      // select query
      val selectStmt = select()
        .all()
        .from(cassandraKeyspace, "blocks")
        .where(QueryBuilder.eq("miner", miner)).and(QueryBuilder.eq("id", id))
        .limit(1)

      val resultSet = MinerFactory.session.execute(selectStmt)
      val row = resultSet.one()

      if (row != null) {
        // get transactions
        val t = row.getSet("transactions", classOf[UDTValue]).asScala.map(t =>
          Trans(t.getUUID("uid"),
            t.getString("type"),
            t.getString("from_address"),
            t.getString("to_address"),
            t.getInt("amount"),
            t.getString("digsig"),
            t.getLong("timestamp")
          )
        ).toList

        // get signatures
        val s = row.getSet("signatures", classOf[UDTValue]).asScala.map(s =>
          Signature(s.getString("miner"), s.getString("digsig"))
        ).toList

        // create block
        Option(
          Block(miner,
            id,
            t,
            row.getLong("timestamp"),
            row.getString("merkle_root"),
            row.getString("pre_hash"),
            row.getString("hash"),
            s)
        )
      }
      else None
    }

    def updateBlockSignature(block: Block, signature: Signature): Unit = {
      // signature type
      val sigType = MinerFactory.cluster.getMetadata.getKeyspace(cassandraKeyspace).getUserType("signature")

      // signature
      val sig = sigType.newValue.setString("miner", signature.miner).setString("digsig", signature.digsig)

      // existing signatures + new signature
      val sigs = block.signatures.map(s =>
        sigType.newValue
          .setString("miner", s.miner)
          .setString("digsig", s.digsig)
      ) :+ sig

      // update query
      val statement = QueryBuilder.update(cassandraKeyspace, "blocks")
        .`with`(QueryBuilder.add("signatures", sig))
        .where(QueryBuilder.eq("miner", block.miner)).and(QueryBuilder.eq("id", block.id))

      MinerFactory.session.execute(statement)
    }

    def buildSearch(id: String): Unit = {
      val builder = QueryBuilders.boolQuery()

      val searchBuilder = client
        .prepareSearch("transactions")
        .setTypes("transactions")
        .setQuery(builder)

      val t1 = System.currentTimeMillis()
      logger.debug(s"start search ---- $t1")

      // build filter
      builder.must(QueryBuilders.matchQuery("from_address", "2222"))
      //builder.must(QueryBuilders.matchQuery("id", "979592a0-a114-11e8-9e0a-b5f523cb454b"))
      //builder.must(QueryBuilders.matchQuery("from_zaddress", id))

      val resp = searchBuilder.execute().actionGet()
      val hits = resp.getHits.getHits

      val h = hits.head.getSourceAsMap.get("from_address").asInstanceOf[String]
      val t2 = System.currentTimeMillis()
      logger.debug(s"end search ---- $t2")
      logger.debug(s"time diff ---- ${t2 - t1}")

      logger.info(s"histor $h")
      logger.info(s"histor ${hits.size}")
    }
  }

}

//object M extends App with ChainDbCompImpl {
//  chainDb.buildSearch("2323")
//}

