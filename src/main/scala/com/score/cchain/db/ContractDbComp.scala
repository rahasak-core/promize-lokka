package com.score.cchain.db

import com.score.cchain.protocol.Account

trait ContractDbComp {

  val contractDb: ContractDb

  trait ContractDb {
    def init(account: Account)
    def put(address: String, amount: Int)
    def get(address: String): Option[Account]
  }

}
