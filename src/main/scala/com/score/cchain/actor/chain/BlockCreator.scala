package com.score.cchain.actor.chain

import akka.actor.{Actor, Props}
import com.score.cchain.config.{AppConf, KafkaConf}
import com.score.cchain.db.ChainDbCompImpl
import com.score.cchain.protocol.{Signature, Trans}
import com.score.cchain.util.{BlockFactory, RSAFactory, SenzLogger}

import scala.concurrent.duration._

object BlockCreator {

  case class Create()

  def props = Props(classOf[BlockCreator])

}

class BlockCreator extends Actor with ChainDbCompImpl with AppConf with KafkaConf with SenzLogger {

  import BlockCreator._
  import context._

  // contract actory actor
  val contractor = context.actorSelection("/user/Contractor")

  // start to create blocks
  self ! Create

  override def receive: Receive = {
    case Create =>
      // take trans, preHash from db and create block
      val trans = chainDb.getTrans
      if (trans.nonEmpty) {
        processTrans(trans)

        // new block
        val preHash = chainDb.getPreHash.getOrElse("")
        val block = BlockFactory.initBlock(trans, preHash)

        // create block
        // set block hash as the preHash
        // delete all transaction saved in the block from trans table
        chainDb.createBlock(block)
        chainDb.deletePreHash()
        chainDb.createPreHash(block.hash)
        chainDb.deleteTrans(block.transactions)

        // finally sign block
        val sig = RSAFactory.sign(block.hash)
        chainDb.updateBlockSignature(block, Signature(senzieName, sig))

        logger.debug("block created and published to kafka")
      } else {
        logger.debug("No trans to create block" + context.self.path)
      }

      // reschedule to create
      context.system.scheduler.scheduleOnce(miningInterval.seconds, self, Create)
  }

  def processTrans(trans: List[Trans]): Unit = {
    // process trans via contractor
    trans.foreach(t => contractor ! t)
  }

}
