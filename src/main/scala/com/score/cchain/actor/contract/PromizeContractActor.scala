package com.score.cchain.actor.contract

import akka.actor.{Actor, Props}
import com.score.cchain.db.ContractDbCompImpl
import com.score.cchain.protocol.{Account, Trans}
import com.score.cchain.util.SenzLogger


object PromizeContractActor {

  case class Init(account: String, balance: Int)

  case class Put(account: String, amount: Int)

  case class Get(account: String)

  def props = Props(classOf[PromizeContractActor])

}

class PromizeContractActor extends Actor with ContractDbCompImpl with SenzLogger {

  import PromizeContractActor._

  override def receive: Receive = {
    case trans: Trans =>
      if (trans.typ == "INIT") {
        // means init trans
        // add balance to acc
        self ! Init(trans.fromAddress, trans.amount)
      } else if (trans.typ == "PUT") {
        // check balance to transfer
        // credit amount -> to
        self ! Put(trans.toAddress, trans.amount)

        // debit amount -> from
        self ! Put(trans.fromAddress, -trans.amount)
      }
    case Init(acc, bal) =>
      // create account and add balance
      contractDb.get(acc) match {
        case Some(a) =>
          contractDb.put(acc, a.balance + bal)
        case None =>
          contractDb.init(Account(acc, bal))
      }
    case Put(acc, amount) =>
      // add amount to give account
      contractDb.get(acc) match {
        case Some(a) =>
          contractDb.put(acc, a.balance + amount)
        case None =>
          // invalid account
      }
    case Get(acc) =>
      // get balance from account
      contractDb.get(acc)
  }

}
